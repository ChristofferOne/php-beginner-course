# Grundläggande PHP-tekniker för Nybörjare

## 1. Variabler

Variabler i PHP deklareras med **$**-tecknet följt av variabelnamnet. Variabler kan innehålla olika typer av data som strängar, heltal och flyttal.

### Exempel:
```php
$namn = "Sara";
$ålder = 25;
```

## 2. Matematiska Operationer

PHP stöder grundläggande matematiska operationer som addition (**+**), subtraktion (**-**), multiplikation (**\***), och division (**/**).

### Exempel:
```php
$summa = 10 + 5;  // 15
```

## 3. Arrays

En array är en samling värden. Det finns flera typer av arrays, inklusive indexbaserade och associativa arrays.

### Exempel:
```php
$frukter = array("Äpple", "Banan", "Körsbär");
```

## 4. Loopar

Loopar används för att upprepa kod. De vanligaste looparna i PHP är **for**, **foreach**, och **while**.

### Exempel:
```php
for ($i = 1; $i <= 10; $i++) {
  echo $i;
}
```

## 5. Villkorssatser

Villkorssatser används för att utföra olika åtgärder beroende på olika förhållanden. **if-else** och **switch** är vanliga i PHP.

### Exempel:
```php
if ($ålder >= 18) {
  echo "Du är myndig.";
} else {
  echo "Du är inte myndig.";
}
```

## 6. Funktioner

Funktioner är block av kod som kan kallas upp flera gånger. Funktioner deklareras med nyckelordet **function**.

### Exempel:
```php
function addera($x, $y) {
  return $x + $y;
}
```

## 7. Associativa Arrays

Associativa arrays har nycklar och värden. Nycklarna används för att hitta värden i arrayen.

### Exempel:
```php
$person = array("namn" => "Sara", "ålder" => 25, "yrke" => "Ingenjör");
```

## 8. Formulär och POST

Formulär används för att samla indata från användaren. **POST** är en metod för att skicka data till servern.

### Exempel:
```html
<form action="handle_form.php" method="post">
  Namn: <input type="text" name="namn">
  <input type="submit" value="Skicka">
</form>
```

